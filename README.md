# README #

Usage: ./main.py

### What is this repository for? ###

* Web Site Dumper dump website from indicated url
* Version 0.3

### How do I get set up? ###

* Change rootUrl to the root url of the website
* Change loginUrl to the url where the login form is hidden.
** To find it:
    1. Dowload HttpFox (for firefox...) 
    2. Start it in it's own windows
    3. Press start
    4. Try to login
    5. In HttpFox find the POST method (green) and you have the url corresponding in the URL field
* Dependencies: Python modules requests, getpass and magic (pip install ftw)


### Who do I talk to? ###

* You don't talk shut up. I do this for fun...

### Where can I find true manual ###

* RTFC :D
#! /usr/bin/python
#
#   Test for Website dumper
#

import requests
import queue
import getpass
import re
import os
import magic

# set these to whatever your account is in login.conf file
username = input("Username: ")
password = getpass.getpass('Password for {0}: '.format(username))

# Here is the login url BE CAREFUL TO WHAT YOU PUT HERE (httpfox can help)
loginUrl = 'https://intra.acu.epita.fr/users/users/login?q=%2Fusers%2Fusers%2Flogin'
rootUrl = "https://intra.acu.epita.fr"  # Root url of the website


## Colors for pretty print

HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'


def dump():
    # Start up: Here reference all parameters
    # in the post request that you need
    p = ParseUrl()
    payload = {
        '_method' : "POST",                 # the method
        'data[User][username]' : username,  # Change the fields name by the
        'data[User][pwd]' : password,       # ones in the login CSS input
        'data[User][password]' : password   # form (or see the doc once it will be done)
    }

    # Open a session
    with requests.Session() as s:
        # Attempt to login
        r = s.post(loginUrl, data=payload)

        # Init parsing list with root url
        r = s.get(rootUrl)
        p.parse(r.text)
        follow = True
        is_html = True
        # Parse all website
        while (p.urlQueue != []):
            root = rootUrl          # Local root
            pwd = p.urlQueue.pop()
            print(ENDC + "---> Working with: {0}".format(pwd))
            if (pwd[:1] != "/"):    # It's a new url and we remove the url part
                root = re.search("http://(.+?)/", pwd)
                if not root:
                    continue
                root = "http://" + root.group(1)
                pwd = p.remove(pwd, root)
                follow = False

            f = p.getPath(pwd)
            print(ENDC + "---> Access to: " + UNDERLINE + "{0}".format(root + pwd))
            try:    # Avoid abort or connection error
                page = (s.get(root + pwd))
            except:
                print(FAIL  + "--> Fail to access " +
                        UNDERLINE + "{0}".format(root + pwd))
            ## Determine if page is html or something else
            if (magic.from_buffer(page.content, mime = True) == b'text/html'):
                is_html = True
            else:
                follow = False
                is_html = False
            ## Parse and write file content
            if follow:
                p.parse(page.text)
            if os.path.isfile(f):
                print(WARNING + "--> File already exists")
                continue
            print(ENDC + "---> Writing file: {0}".format(f))
            if is_html:
                content = open(f, 'w+')
                content.write(page.text)
                content.close()
            else:
                content = open(f, 'wb+')
                content.write(page.content)
                content.close()
            # Reset url following
            follow = True

## Url Parser stocking all visited and url to visit
class ParseUrl(object):

    def __init__(self):
        self.urlList = []
        self.urlQueue = []

    def remove(self, string, substr):   # Remove substring in string
        lindex = string.find(substr)
        rindex = lindex + len(substr)
        return string[0:lindex] + string[rindex:]


    # FIXME: Add https redirection
    def parse(self, page):  # Parse the page and fill urlList and urlQueue
        # Find href
        m = re.search('href=\"(.+?)\"', page)
        while m:
            found = m.group(1)
            # Remove href in the page
            page = self.remove(page, "href=\"" + found + "\"")
            if (not (found in self.urlList)) and (found[:1] == '/' or
                    (len(found) > 7 and (found[:7] =="http://"))):
                self.urlList.append( found)
                self.urlQueue.append(found)
            m = re.search('href=\"(.+?)\"', page)
        # Find src
        m = re.search('src=\"(.+?)\"', page)
        while m:
            found = m.group(1)
            # Remove src in the page
            page = self.remove(page, "src=\"" + found + "\"")
            if (not (found in self.urlList)) and (found[:1] == '/' or
                    (len(found) > 7 and (found[:7] =="http://"))):
                self.urlList.append( found)
                self.urlQueue.append(found)
            m = re.search('src=\"(.+?)\"', page)
        # Remove some strash href (looking for a better idea)
        try:
            self.urlList.remove('#')
            self.urlQueue.remove('#')
            self.urlList.remove('/')
            self.urlQueue.remove('/')
        except:
            pass

    def getPath(self, ref):     # Return the path to write the index file
        if (re.search("\.(.+?)$", ref)): # It's a file
            dirs = ref.split('/')
            dirPath = "/".join(dirs[:-1]) # remove the last word
            dirPath = "." + dirPath
            print(ENDC + "---> Creating directory: {0}".format(dirPath))
            try:
                os.makedirs(dirPath)
            except FileExistsError:
                print(WARNING + "--> Already created")
            return "." + ref
        else:            # It's a redirection so we create an index
            dirPath = "." + ref
            print(ENDC + "---> Creating directory: {0}".format(dirPath))
            try:
                os.makedirs(dirPath)
            except FileExistsError:
                print(WARNING + "--> Already created")
            return "." + ref + "/index.html"


def main():
    dump()

main()
